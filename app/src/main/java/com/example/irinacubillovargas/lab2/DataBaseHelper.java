package com.example.irinacubillovargas.lab2;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Irina Cubillo Vargas on 9/5/2018.
 */

public class DataBaseHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 2;

    public static final String DATABASE_NAME = "base.db";

    public DataBaseHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    public DataBaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DataBaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version,
                          DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DataBaseContract.SQL_CREATE_SEDE);
        sqLiteDatabase.execSQL("insert into SEDE (NOMBRE, DESCRIPCION, LONGITUD, LATITUD) values ('San Carlos','Esta sede, ubicada en Santa Clara de San Carlos, en la región tropical húmeda, se encuentra a 105 kilómetros de San José y a 170 metros sobre el nivel del mar, con una temperatura media anual de 26ºC', '-84.5057934 ', '10.3652482')");
        sqLiteDatabase.execSQL("insert into SEDE (NOMBRE, DESCRIPCION, LONGITUD, LATITUD) values ('San José','El Centro Académico de San José se ubica en Barrio Amón, entre calles 5 y 7 y entre avenidas 9 y 11.  Sus aulas, oficinas administrativas, biblioteca y áreas recreativas se distribuyen entre edificios nuevos y casas antiguas con el valor histórico propio del barrio', '-84.0695866', '9.9296424')");
        sqLiteDatabase.execSQL("insert into SEDE (NOMBRE, DESCRIPCION, LONGITUD, LATITUD) values ('Cartago','La Sede Central del Tecnológico de Costa Rica se encuentra en Cartago, una ciudad que se ubica 24 kilómetros al sureste de San José, a una altitud de 1,435 metros sobre el nivel del mar y con un clima tropical húmedo, aunque suele ser más templado debido a su ubicación geográfica y altura, con lluvias moderadas y temperaturas frescas que varían entre 15 y 26 grados centígrados la mayor parte del año', '-83.9125516', '9.8564963')");
        sqLiteDatabase.execSQL("insert into SEDE (NOMBRE, DESCRIPCION, LONGITUD, LATITUD) values ('Limón','La provincia de Limón se encuentra en un momento de expansión producto de la inversión pública y privada. La región Huetar Caribe contará con obras de infraestructura que apuntan hacia un desarrollo social, entre ellas: la Megaterminal APM, la nueva refinadora de Recope, la Planta Hidroeléctrica del Reventazón y la ampliación de la ruta 32', '-83.6893576', '10.1026622')");
        sqLiteDatabase.execSQL("insert into SEDE (NOMBRE, DESCRIPCION, LONGITUD, LATITUD) values ('Alajuela','El Centro Académico se ubica en Desamparados de Alajuela, a 21 kilómetros de San José. Se trata de una localidad en la región intertropical, a 960 metros sobre el nivel del mar y con una temperatura media anual de 22,3ºC. La precipitación pluvial media es de 2.069 mm. anuales, con una estación seca de enero a mediados de marzo y una estación lluviosa de mediados de marzo a diciembre', '-84.1971997', '10.0198026')");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(DataBaseContract.SQL_DELETE_SEDE);
        onCreate(sqLiteDatabase);
    }
}
