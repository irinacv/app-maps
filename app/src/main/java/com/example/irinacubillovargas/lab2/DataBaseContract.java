package com.example.irinacubillovargas.lab2;

import android.provider.BaseColumns;

/**
 * Created by Irina Cubillo Vargas on 9/5/2018.
 */

public class DataBaseContract {
    public static class DataBaseEntry implements BaseColumns {

        public static final String TABLE_SEDE = "SEDE";
        public static final String COlUMN_SEDE_NOMBRE = "NOMBRE";
        public static final String COlUMN_SEDE_DESCRIPCION = "DESCRIPCION";
        public static final String COLUMN_SEDE_LONGITUD = "LONGITUD";
        public static final String COLUMN_SEDE_LATITUD = "LATITUD";


    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    public static final String SQL_CREATE_SEDE =
            "CREATE TABLE " + DataBaseEntry.TABLE_SEDE + " (" +
                    DataBaseEntry.COlUMN_SEDE_NOMBRE + TEXT_TYPE + "PRIMARY KEY," +
                    DataBaseEntry.COlUMN_SEDE_DESCRIPCION + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_SEDE_LONGITUD + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_SEDE_LATITUD + TEXT_TYPE + " )";

    public static final String SQL_DELETE_SEDE =
            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_SEDE;

}
