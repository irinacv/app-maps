package com.example.irinacubillovargas.lab2;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by Irina Cubillo Vargas on 9/5/2018.
 */

public class Sede {
    private String nombre;
    private String descripcion;
    private String longitud;
    private String latitud;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public ArrayList<Sede> leerSedes(Context context){
        DataBaseHelper DatabaseHelper = new DataBaseHelper(context);

        // Obtiene la base de datos en modo lectura
        SQLiteDatabase db = DatabaseHelper.getReadableDatabase();

        // Define cuales columnas quiere solicitar // en este caso todas las de la clase
        String[] projection = {
                DataBaseContract.DataBaseEntry.COlUMN_SEDE_NOMBRE,
                DataBaseContract.DataBaseEntry.COlUMN_SEDE_DESCRIPCION,
                DataBaseContract.DataBaseEntry.COLUMN_SEDE_LONGITUD,
                DataBaseContract.DataBaseEntry.COLUMN_SEDE_LATITUD
        };

        // Filtro para el WHERE
        //String selection = DataBaseContract.DataBaseEntry.COLUMN_RUTINA_NOMBRE_USER+ " = ?";
        //String[] selectionArgs = {usuario};

        // Resultados en el cursor
        Cursor cursor = db.query(
                DataBaseContract.DataBaseEntry.TABLE_SEDE, // tabla
                projection, // columnas
                null, // where
                null, // valores del where
                null, // agrupamiento
                null, // filtros por grupo
                null // orden
        );

        final ArrayList<Sede> misListas = new ArrayList<>();
        if(cursor.moveToFirst()) {
            do {
                Sede miLista = new Sede();
                miLista.setNombre(cursor.getString(cursor.getColumnIndexOrThrow(
                        DataBaseContract.DataBaseEntry.COlUMN_SEDE_NOMBRE)));
                miLista.setDescripcion(cursor.getString(cursor.getColumnIndexOrThrow(
                        DataBaseContract.DataBaseEntry.COlUMN_SEDE_DESCRIPCION)));
                miLista.setLongitud(cursor.getString(cursor.getColumnIndexOrThrow(
                        DataBaseContract.DataBaseEntry.COLUMN_SEDE_LONGITUD)));
                miLista.setLatitud(cursor.getString(cursor.getColumnIndexOrThrow(
                        DataBaseContract.DataBaseEntry.COLUMN_SEDE_LATITUD)));
                misListas.add(miLista);
            } while (cursor.moveToNext());
        }
        return misListas;
    }
}
